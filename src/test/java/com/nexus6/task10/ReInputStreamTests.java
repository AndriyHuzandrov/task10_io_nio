package com.nexus6.task10;

import com.nexus6.task10.pushback.ReInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;

public class ReInputStreamTests {
  private byte[] source = new byte[1];

  @Test
  public void PushBackByteGet() throws IOException {
    ReInputStream testedStream =
        new ReInputStream(new ByteArrayInputStream(source), 10);
    testedStream.pushBack(5);
    int result = testedStream.read();
    Assert.assertEquals(5, result);
  }
}
