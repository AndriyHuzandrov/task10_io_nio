package com.nexus6.task10;

import com.nexus6.task10.bookpack.Shelf;
import com.nexus6.task10.utils.TxtUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import org.junit.Assert;
import org.junit.Test;

public class SerializeObjTests {

  @Test
  public void TestDeserialize() {
    String oFile = "taskdir/Object.dat";
    Shelf shelfFromFile;
    int rowDeserialized = 0;
    try(ObjectInputStream dataIn = new ObjectInputStream(new FileInputStream(oFile))) {
      shelfFromFile = (Shelf) dataIn.readObject();
      rowDeserialized = shelfFromFile.getRow();
    } catch (FileNotFoundException e) {
      TxtUtils.appLog.error(TxtUtils.BAD_FILE_NAME);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    } catch (ClassNotFoundException e) {
      TxtUtils.appLog.error("Unknown error");
    }
    Assert.assertEquals(10, rowDeserialized);
  }
}
