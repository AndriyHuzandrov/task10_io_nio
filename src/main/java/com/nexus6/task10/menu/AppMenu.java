package com.nexus6.task10.menu;

import com.nexus6.task10.comm.CommentsParseComm;
import com.nexus6.task10.comm.CustomNIOBufferComm;
import com.nexus6.task10.comm.Executable;
import com.nexus6.task10.comm.ExitAppComm;
import com.nexus6.task10.comm.FormTreeComm;
import com.nexus6.task10.comm.PushBackComm;
import com.nexus6.task10.comm.ReadFileStatsComm;
import com.nexus6.task10.comm.SerializeObjComm;
import com.nexus6.task10.utils.TxtUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.logging.log4j.Logger;

public class AppMenu {
  private BufferedReader br;
  private Logger mainLog;
  private Map<String, Executable> exeMenu;

  public AppMenu() {
    br = new BufferedReader(new InputStreamReader(System.in));
    mainLog = TxtUtils.appLog;
    exeMenu = new LinkedHashMap<>();
    prepareExeMenu();
  }

  public void getChoice() {
    showTxtMenu(TxtUtils.getTxtMenu());
    int menuBound = exeMenu.size() - 1;
    try {
      String choice = br.readLine();
      if(choice.trim().matches("[0-" + menuBound + "]")) {
        processChoice(choice);
      } else {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      mainLog.error(TxtUtils.CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      mainLog.warn(TxtUtils.BAD_MENU_ITM);
      getChoice();
    }
  }

  private void processChoice(String choice) {
    exeMenu
        .entrySet()
        .stream()
        .filter(i -> i.getKey().equals(choice))
        .forEach(i -> i.getValue().execute());
    showPause();
    getChoice();
  }

  private void showPause() {
    mainLog.info(TxtUtils.PAUSE_MSG);
    try {
      br.readLine();
    } catch (IOException e) {
      mainLog.error(TxtUtils.CONS_INP_ERROR);
    }
  }

  private void showTxtMenu(Supplier<Stream<String>> menu) {
    menu
        .get()
        .map(i -> i + "\n")
        .forEach(mainLog::info);
    mainLog.info(TxtUtils.SEL_MENU_ITM);
  }

  private void prepareExeMenu() {
    exeMenu.put("0", new ExitAppComm());
    exeMenu.put("1", new SerializeObjComm());
    exeMenu.put("2", new ReadFileStatsComm());
    exeMenu.put("3", new PushBackComm());
    exeMenu.put("4", new CommentsParseComm());
    exeMenu.put("5", new FormTreeComm());
    exeMenu.put("6", new CustomNIOBufferComm());
  }
}
