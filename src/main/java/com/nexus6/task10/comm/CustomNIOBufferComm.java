package com.nexus6.task10.comm;

import com.nexus6.task10.custombuffer.CoddedBuffer;
import com.nexus6.task10.utils.TxtUtils;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class CustomNIOBufferComm extends BlankComm {
  private Path codedFile;
  private Path decodedFile;
  private CoddedBuffer testBuffer;

  public CustomNIOBufferComm() {
    codedFile = Paths.get(appConfig.getProperty("codeFile"));
    decodedFile = Paths.get(appConfig.getProperty("decFile"));
    testBuffer = new CoddedBuffer();

  }

  public void execute() {
    try(FileChannel codedChannel =
        (FileChannel) Files.newByteChannel(codedFile, StandardOpenOption.CREATE,
                                                      StandardOpenOption.WRITE,
                                                      StandardOpenOption.READ);
        FileChannel decodedChannel =
        (FileChannel) Files.newByteChannel(decodedFile, StandardOpenOption.CREATE,
                                                        StandardOpenOption.WRITE)){
      testBuffer.putString(TxtUtils.SAMPLE);
      testBuffer.moveBufferedDataTo(codedChannel);
      testBuffer.decodeDataTo(codedChannel, decodedChannel);

    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
  }
}
