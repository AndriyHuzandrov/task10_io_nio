package com.nexus6.task10.comm;

public class ExitAppComm implements Executable {

  public void execute() {
    System.exit(0);
  }
}
