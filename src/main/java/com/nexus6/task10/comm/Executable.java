package com.nexus6.task10.comm;

public interface Executable {
  void execute();
}
