package com.nexus6.task10.comm;

import com.nexus6.task10.bookpack.Book;
import com.nexus6.task10.bookpack.Shelf;
import com.nexus6.task10.utils.TxtUtils;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeObjComm extends BlankComm {

  public void execute() {
    Shelf shelf1 = new Shelf(10, "history");
    Book b1 = new Book("History of ancient ages", 765);
    shelf1.setBookOn(b1);
    try(ObjectOutputStream dataOut =
        new ObjectOutputStream(
            new BufferedOutputStream(
                new FileOutputStream(appConfig.getProperty("objFile"))))){
      dataOut.writeObject(shelf1);
    } catch (FileNotFoundException e) {
      TxtUtils.appLog.error(TxtUtils.BAD_FILE_NAME);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
  }
}
