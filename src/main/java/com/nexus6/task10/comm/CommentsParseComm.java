package com.nexus6.task10.comm;

import static com.nexus6.task10.utils.NumUtils.EOF;
import com.nexus6.task10.utils.TxtUtils;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.StringTokenizer;

public class CommentsParseComm extends BlankComm {


  public void execute() {
    String source = getFileAsString(appConfig.getProperty("javaFile"));
    Collections.list(new StringTokenizer(source, "\n"))
        .stream()
        .map(s -> (String)s)
        .filter(s -> s.contains("//"))
        .forEach(System.out::println);
  }

  private String getFileAsString(String fName) {
    int b;
    StringBuilder sb = new StringBuilder();
    try(FileReader input = new FileReader(fName)) {
      while((b = input.read()) != EOF) {
        sb.append(Character.toChars(b));
      }
    } catch (FileNotFoundException e) {
      TxtUtils.appLog.error(TxtUtils.BAD_FILE_NAME);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
    return sb.toString();
  }
}
