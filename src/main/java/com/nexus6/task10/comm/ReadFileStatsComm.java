package com.nexus6.task10.comm;

import com.nexus6.task10.utils.TxtUtils;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static com.nexus6.task10.utils.NumUtils.*;

public class ReadFileStatsComm extends BlankComm {
  private float unbufReadTime;
  private float buffReadTime;
  private float smallBufTime;
  private float normalBufTime;
  private float bigBuffTime;

  public void execute() {
    TxtUtils.appLog.info(TxtUtils.START_UB_READ);
    try(FileInputStream unbufferedInput =
        new FileInputStream(appConfig.getProperty("largeFile"));
        BufferedInputStream bufferedInput =
            new BufferedInputStream(
                new FileInputStream(appConfig.getProperty("largeFile")));
        BufferedInputStream smallInput =
        new BufferedInputStream(
            new FileInputStream(appConfig.getProperty("largeFile")), SMALL_BUFFER);
        BufferedInputStream normalInput =
        new BufferedInputStream(
            new FileInputStream(appConfig.getProperty("largeFile")), NORMAL_BUFFER);
        BufferedInputStream bigInput =
        new BufferedInputStream(
            new FileInputStream(appConfig.getProperty("largeFile")), LARGE_BUFFER)) {
      unbufReadTime = getFileReadTime(unbufferedInput);
      TxtUtils.appLog.info(TxtUtils.START_B_READ);
      buffReadTime = getFileReadTime(bufferedInput);
      TxtUtils.appLog.info(TxtUtils.BS + " " + TxtUtils.START_B_READ);
      smallBufTime = getFileReadTime(smallInput);
      normalBufTime = getFileReadTime(normalInput);
      bigBuffTime = getFileReadTime(bigInput);
    } catch (FileNotFoundException e) {
      TxtUtils.appLog.error(TxtUtils.BAD_FILE_NAME);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
    prepareStats();
  }

  private float getFileReadTime(InputStream input) throws IOException {
    int target = 0;
    long timeBefore = System.currentTimeMillis();
    do{
      target = input.read();
    }while(target != EOF);
    TxtUtils.appLog.info(TxtUtils.DONE);
    return (float)(System.currentTimeMillis() - timeBefore)/1000;
  }

  private void prepareStats() {
    TxtUtils.appLog.info(String.format(TxtUtils.STAT_TAB_HEADER,
                                        TxtUtils.READ_OP,
                                          TxtUtils.TIME));
    TxtUtils.appLog.info(String.format(TxtUtils.STAT_TAB_ROW,
                                        TxtUtils.UBR,
                                        getUnbufReadTime()));
    TxtUtils.appLog.info(String.format(TxtUtils.STAT_TAB_ROW,
                                        TxtUtils.BR,
                                        getBuffReadTime()));
    TxtUtils.appLog.info(String.format(TxtUtils.STAT_TAB_ROW,
                                        TxtUtils.BS,
                                        getSmallBufTime()));
    TxtUtils.appLog.info(String.format(TxtUtils.STAT_TAB_ROW,
                                        TxtUtils.BN,
                                        getNormalBufTime()));
    TxtUtils.appLog.info(String.format(TxtUtils.STAT_TAB_ROW,
                                        TxtUtils.BB,
                                        getBigBuffTime()));
  }

  public float getUnbufReadTime() {
    return unbufReadTime;
  }

  public float getBuffReadTime() {
    return buffReadTime;
  }

  public float getSmallBufTime() {
    return smallBufTime;
  }

  public float getNormalBufTime() {
    return normalBufTime;
  }

  public float getBigBuffTime() {
    return bigBuffTime;
  }
}
