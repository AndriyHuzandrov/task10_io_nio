package com.nexus6.task10.comm;

import com.nexus6.task10.utils.TxtUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.NoSuchElementException;

public class FormTreeComm extends BlankComm {
  private BufferedReader br;
  private File currNode;
  private StringBuilder sb;


  public FormTreeComm() {
    br = new BufferedReader(new InputStreamReader(System.in));
    currNode = new File(appConfig.getProperty("rootPath"));
    sb = new StringBuilder();
  }

  public void execute() {
    showMenu();
    execChoice();
  }

  private void execChoice() {
    TxtUtils.appLog.info(TxtUtils.COMM);
    try {
      String[] input = br.readLine().split(" ");
      switch(input[0]) {
        case "cd":
          File temNode = currNode;
          currNode = new File(input[1].trim());
          if(!currNode.isDirectory()) {
            currNode = temNode;
            throw new NoSuchElementException();
          }
          break;
        case "ls":
          printDir(currNode.getPath());
          break;
        case "tree":
          TxtUtils.appLog.info(buildTree(currNode, ""));
          break;
        case "logout":
          return;
          default:
            TxtUtils.appLog.warn(TxtUtils.BAD_MENU_ITM);
            break;
      }
      execChoice();
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.CONS_INP_ERROR);
    } catch (NoSuchElementException e) {
      TxtUtils.appLog.warn(TxtUtils.NOT_DIR);
      execChoice();
    }
  }
  private void printDir(String path) {
    File node = new File(path);
      Arrays.stream(node.listFiles())
          .forEach(file -> TxtUtils.appLog.info(file.getName() + "\n"));
  }

  private String buildTree(File node, String delimiter) {
    File[] fileList = node.listFiles();
    for(File currNode : fileList) {
      if(currNode.isDirectory()) {
        sb.append(delimiter);
        sb.append(currNode.getName());
        sb.append("\n");
        delimiter += "|\t";
        buildTree(currNode, delimiter);
      } else {
        sb.append(delimiter);
        sb.append("+-");
        sb.append(currNode.getName());
        sb.append("\n");
      }
    }
    return sb.toString();
  }

  private void showMenu() {
    TxtUtils
        .getFileMenu()
        .get()
        .map(item -> item + "\n")
        .forEach(TxtUtils.appLog::info);
  }
}
