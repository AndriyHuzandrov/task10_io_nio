package com.nexus6.task10.comm;

import com.nexus6.task10.utils.TxtUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

abstract class BlankComm implements Executable {
  Properties appConfig;

  BlankComm() {
    appConfig = new Properties();
    prepareProps();
  }
  private void prepareProps() {
    try(FileInputStream fr = new FileInputStream(TxtUtils.CONF_FILENAME)) {
      appConfig.load(fr);
    } catch (FileNotFoundException e) {
      TxtUtils.appLog.error(TxtUtils.BAD_FILE_NAME);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
  }
  abstract public void execute();

}
