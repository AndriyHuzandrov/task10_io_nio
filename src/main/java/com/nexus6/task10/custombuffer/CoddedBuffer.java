package com.nexus6.task10.custombuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class CoddedBuffer {
  private ByteBuffer mainBuffer;
  private byte[] dataArray;
  private int key;

  public CoddedBuffer() {
    key = 50;
    dataArray = new byte[128];
    mainBuffer = ByteBuffer.wrap(dataArray);
  }
  public void moveBufferedDataTo(FileChannel ch) throws IOException {
    mainBuffer.flip();
    ch.write(mainBuffer);
  }

  private byte codeByte(char c){
    return (byte) (c ^ key);
  }

  public void putString(String str) {
    for(int i = 0; i < str.length(); i++) {
      mainBuffer.put(codeByte(str.charAt(i)));
    }
  }

  public void decodeDataTo(FileChannel fromChan, FileChannel toChan) throws IOException {
    fromChan.read(mainBuffer);
    mainBuffer.flip();
    for(int i = 0; i < mainBuffer.limit(); i++) {
      mainBuffer.put(i, codeByte((char) mainBuffer.get(i)));
    }
    toChan.write(mainBuffer);
  }
}
