package com.nexus6.task10.bookpack;

public class Book {
  String title;
  int pageNumber;

  public Book(String t, int p) {
    this.title = t;
    this.pageNumber = p;
  }
}
