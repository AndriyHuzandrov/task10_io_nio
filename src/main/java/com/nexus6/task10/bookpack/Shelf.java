package com.nexus6.task10.bookpack;

import java.io.Serializable;

public class Shelf implements Serializable {
  private int row ;
  private String category;
  private transient Book bookOn;

  public Shelf(int row, String category) {
    this.row = row;
    this.category = category;
  }

  public void setBookOn(Book bookOn) {
    this.bookOn = bookOn;
  }

  public int getRow() {
    return row;
  }
}
