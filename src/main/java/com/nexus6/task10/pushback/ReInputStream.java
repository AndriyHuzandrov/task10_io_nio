package com.nexus6.task10.pushback;

import com.nexus6.task10.utils.TxtUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public class ReInputStream extends InputStream implements Closeable {
  private InputStream inputSource;
  private byte[] mainBuffer;
  private int location;

  public ReInputStream(InputStream i, int size) {
    inputSource = i;
    if(size <= 0) {
      TxtUtils.appLog.warn(TxtUtils.BAD_ARR_SIZE);
      mainBuffer = new byte[255];
    } else {
      mainBuffer = new byte[size];
    }
    location = mainBuffer.length;
  }

  @Override
  public int read() throws IOException {
    if(inputSource == null) {
      throw new IOException("Data stream lost");
    }
    if(location < mainBuffer.length) {
      return mainBuffer[location++];
    }
    return inputSource.read();
  }

  public void pushBack(int data) {
    if(location == 0) {
      throw new IndexOutOfBoundsException("Buffer overfilled");
    }
    mainBuffer[--location] = (byte) data;
  }

  @Override
  public void close() throws IOException {
    inputSource.close();
    mainBuffer = null;
  }
}
