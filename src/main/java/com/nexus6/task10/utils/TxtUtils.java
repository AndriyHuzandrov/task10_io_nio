package com.nexus6.task10.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TxtUtils {
  public static final Logger appLog = LogManager.getLogger(TxtUtils.class);
  private static List<String> txtMenu;
  private static List<String> fileMenu;
  public static String CONS_INP_ERROR = "Error reading from console.";
  public static String BAD_MENU_ITM = "Bad menu item";
  public static String SEL_MENU_ITM = "Select menu item: ";
  public static String BAD_FILE_NAME = "No such file/directory";
  public static String FILE_IO_ERR = "File operation error";
  public static String CONF_FILENAME = "src/main/resources/config.properties";
  public static String PAUSE_MSG = "Press Enter to continue\n";
  public static String START_UB_READ = "Unbuffered reading from file\n";
  public static String DONE = "Finishing reading procedure\n";
  public static String STAT_TAB_HEADER = "%30s%10s%n";
  public static String READ_OP = "Read operation";
  public static String TIME = "Time [s]";
  public static String START_B_READ = "Buffered reading from file\n";
  public static String STAT_TAB_ROW = "%30s%10.2f%n";
  public static String BR = "Read from buffered stream";
  public static String UBR = "Read from unbuffered stream";
  public static String BS = "Buffer size 512 KB";
  public static String BN = "Buffer size 1024 KB";
  public static String BB = "Buffer size 20148 KB";
  public static String BAD_ARR_SIZE = "Bad buffer size. Setting up default value.";
  public static String SEE_TEST = "See corresponding test\n";
  public static String COMM = "command: ";
  public static String NOT_DIR = "Not a directory";
  public static String SAMPLE = "This is sample message";



  static {
    txtMenu = new LinkedList<>();
    txtMenu.add("1. Serialize/deserialize object.");
    txtMenu.add("2. Check buffered/unbuffered statistics.");
    txtMenu.add("3. InputStream modification.");
    txtMenu.add("4. Get all comments from a java-code.");
    txtMenu.add("5. Print directory tree.");
    txtMenu.add("6. Test custom Buffer (NIO).");
    txtMenu.add("0. Exit");
    fileMenu = new LinkedList<>();
    fileMenu.add("Available commands");
    fileMenu.add("cd [path] => to change directory");
    fileMenu.add("ls => to show directory list");
    fileMenu.add("tree => to build directories tree");
    fileMenu.add("logout => to exit program");
  }

  private TxtUtils() {}

  public static Supplier<Stream<String>> getTxtMenu() {
    return () -> txtMenu.stream();
  }

  public static Supplier<Stream<String>> getFileMenu() {
    return () -> fileMenu.stream();
  }
}
