package com.nexus6.task10.utils;

public class NumUtils {
  public static int SMALL_BUFFER = 512;
  public static int NORMAL_BUFFER = 1024;
  public static int LARGE_BUFFER = 2048;
  public static int EOF = -1;

  private NumUtils() {}
}
